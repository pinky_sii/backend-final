from flask import Flask, jsonify, request
import logging 
from flask_socketio import SocketIO, join_room, leave_room, send, emit
from flask_cors import CORS
# from flask_pymongo import PyMongo
from pymongo import MongoClient
import uuid, sys, os, json, random
import redis 

# MONGO_URI= os.getenv('MONGO_URI', 'localhost')
# MONGO_PORT = os.getenv('MONGO_PORT', "27017")


MONGO_URI = os.getenv('MONGO_URI', "mongo-service")
MONGO_PORT = os.getenv('MONGO_PORT', "27017")
client = MongoClient(f'mongodb://{MONGO_URI}:{MONGO_PORT}')
mongo = client.mongo
db = mongo.db.final


app = Flask(__name__)

LOG = logging

log = logging.getLogger('werkzeug')
log.disabled = True
app.logger.disabled = True

log = logging.getLogger('engineio')
log.disabled = True
app.logger.disabled = True


INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)


cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
# app.config['MONGO_URI'] = f'mongodb://{MONGO_URI}:{MONGO_PORT}/final'
# mongo = PyMongo(app)
socketio = SocketIO(app, async_mode = 'threading')


REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'redis-service')
REPORT_QUEUE = 'queue:report'
VALIDATE_QUEUE = 'queue:validate'
class RedisResource:

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)


def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.exception('json.loads failed')
            if task:
                callback_func(task)

def subscribe():
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn,
        REPORT_QUEUE,
        lambda task_descr: status_report(named_logging, task_descr))

def construct_winner(winner, loser, data):
	win_info = {}
	winner_info = {}
	loser_info = {}
	# LOG.info(f"data from construct winner {data}")
	# LOG.info(f"{loser}")
	# LOG.info(f"{winner}")
	loser_info["name"] = data[loser]["name"]
	loser_info["score"] = data[loser]["score"]
	winner_info["name"] = data[winner]["name"]
	winner_info["score"] = data[winner]["score"]
	# LOG.info(f"successfully create loser and winner")
	# LOG.info(f"{loser}")
	# LOG.info(f"{winner}")
	win_info["winner"] = winner_info 
	win_info["loser"] = loser_info
	LOG.info(f"win_info from construct winner {win_info}")
	return win_info 

def status_report(logging, task):
	LOG.info(task)
	# Check if the other player concluded flag is true
	# If not go normal flow
	# If true Check if the new score is larger than the opponent
	# if so this player wins
	# if not, keep playing
	# dont change the turn
	uuid = task["_id"]
	opposing_player = "player_one" if task["turn"] == "player_one" else "player_two"
	current_player = "player_two" if task["turn"] == "player_one" else "player_one"
	if task[opposing_player]["concluded"]:
		#don't change turn 
		task["turn"] = "player_two" if task["turn"] == "player_one" else "player_one"
		LOG.info(str(task["turn"]))
		if task[current_player]["score"] > task[opposing_player]["score"]: #this player automatically win 
			LOG.info("PLAYER WIN PLACE CONCLUDE")
			task["winner_info"] = construct_winner(current_player, opposing_player, task)
			LOG.info("success add winner")
			task["status"] = "ended"
			socketio.emit('on_game_loop', task, room=uuid)
			LOG.info("Emitted")

			# mongo.db['room'].delete_one({'_id': uuid})
			return 
	#the normal flow. 
	socketio.emit('on_game_loop', task, room=uuid)
	db.replace_one({'_id': uuid},task)



@app.route('/host', methods=['POST'])
def host_room():
	LOG.info("----BLAH-----")
	uid = str(uuid.uuid4())
	data = None
	body = request.json
	# LOG.info({request.json})
	json_packed = json.dumps(body)
	# LOG.info({json_packed})
	try:
		data = json.loads(json_packed)
	except Exception:
		print("failed")
		pass

	LOG.info(f"{data}")

	info = {'_id': uid,
			"player_one":
				{
				"name": "",
				"blocks": [ {"i": 0, "j": 0, "x": 1, "y": 1, "color": "red"}],
				"score": 1,
				"color":"red",
				"edges": [[0,0,2]],
				"token": 3,
				"concluded": False
				}
			,
			"player_two":
				 {
				 "name": "",
				 "blocks": [{"i": 17, "j": 26, "x": 1, "y": 1, "color": "blue"}],
				 "score": 1,
				 "color":"blue",
				 "edges": [[17,26,2]],
				 "token": 3,
				 "concluded": False 
				 }
			 ,
			 "turn": "player_one",
			 "status": "start",
			 }
	# print("info", info)	
	LOG.info(f"join player {info}")
	info = player_join(info, data.get("name"))
	LOG.info(f"uid is {uid}")

	# print("info", info)
	db.insert_one(info)
	LOG.info(f"successfully insert data in mongo")
	body = {'uid':uid}
	return jsonify(body)

def player_join(data, name):
	if data["player_one"]["name"]=="" :
		print("p1")
		data["player_one"]["name"] = name
	elif data["player_two"]["name"]=="" :
		print("p2")
		data["player_two"]["name"] = name
	else:
		return {}
	return data

@app.route('/join', methods=['POST'])
def join_game():
	data = None
	body = request.json
	json_packed = json.dumps(body)
	try:
		data = json.loads(json_packed)
	except Exception:
		pass
	LOG.info(f"{data}")
	uid = data.get("uid")
	db_data = db.find_one({'_id':uid})
	if db_data :
		if db_data["player_one"]["name"] != "" and db_data["player_two"]["name"] != "":
			return jsonify({'status':'Joined as a spectator'}), 200
		if data.get("name") in [db_data["player_one"]["name"], db_data["player_two"]["name"]]:
			return jsonify({'status':'Name already exist'}),400
		db_data = player_join(db_data, data.get("name"))
		db.replace_one({'_id': db_data['_id']},db_data)
		return jsonify({'data': db_data}),200
	else:
		return jsonify({'status':'ID does not exist'}),400


@socketio.on('connect')
def test_connect():
	print('Client connected')

@socketio.on('disconnect')
def test_disconnect():
	emit('on_disconnected', {'status': 'end'})
	print('Client disconnected')

@socketio.on('join')
def on_join(uuid):
	join_room(uuid)
	print('on join uuid', uuid)
	db_data = db.find_one({'_id':uuid})
	# print("db_data", db_data)
	if db_data:
		db.replace_one({'_id': db_data['_id']},db_data)
		emit('on_join_room', {'data': db_data},room=uuid)
	else:
		emit('on_join_room', {'status':'ID does not exist'},room=uuid)

# @socketio.on('leave')
# def on_leave(uuid):
# 	leave_room(uuid)
# 	send('current user has left the room.', room=room)

@socketio.on('game_loop')
def start_game(uuid):
	data = db.find_one({'_id':uuid})
	emit('on_game_loop', data, room=uuid)

@socketio.on('move')
def move_block(data):
	uuid = data['uuid']
	emit('on_move', {'data': data}, room=uuid)

@socketio.on('roll')
def roll_die(uuid):
	die_1 = random.randint(1,6)
	die_2 = random.randint(1,6)
	data = {
		'x':die_1,
		'y':die_2
		}
	emit ('on_roll', data, room=uuid)

@socketio.on('place')
def place_block(packed):
	uuid = packed['uuid']
	block = packed['block']
	data = db.find_one({'_id':uuid})
	data["placing_block"] = block 
	json_packed = json.dumps(data)
	RedisResource.conn.rpush(
		VALIDATE_QUEUE,
		json_packed)

#using token.
@socketio.on('pass')
def pass_turn(packed):
	uuid = packed["uuid"]
	current_player = packed["turn"]
	data = db.find_one({'_id':uuid})
	token = data[current_player]["token"]
	next_player = "player_two" if data["turn"] == "player_one" else "player_one"
	if data[next_player]["concluded"]:
		next_player = current_player
	if token > 0:
		data[current_player]["token"] -= 1
		data["turn"] = next_player
		db.replace_one({'_id': data['_id']},data)
		emit('on_game_loop',data,room=uuid)
	else:
		emit('on_game_loop',data,room=uuid)

#conclude 
#Check winner. 
@socketio.on('conclude')
def conclude(packed):
	uuid = packed["uuid"]
	data = db.find_one({'_id':uuid})
	current_player = packed["turn"]
	data[current_player]["concluded"] = True 
	enemy_player = "player_two" if data["turn"] == "player_one" else "player_one"
	#the current player have lower score, hence loses.
	LOG.info(str(data[current_player]["score"]))
	LOG.info(str(data[enemy_player]["score"]))
	LOG.info(str(data[current_player]["score"] < data[enemy_player]["score"]))
	if data[current_player]["score"] < data[enemy_player]["score"]:
		LOG.info("PLAYER LOSS CONCLUDE")
		data["winner_info"] = construct_winner(enemy_player, current_player, data)
		LOG.info(data)
		data["status"] = "ended"
		emit('on_game_loop', data, room=uuid)
		LOG.info("EMIT")

		# mongo.db['room'].delete_one({'_id': uuid})

	else:
		#else it's the other player turn. 
		data["turn"] = enemy_player
		emit('on_game_loop', data, room=uuid)
		db.replace_one({'_id': data['_id']},data)

	#check if the current player have lower score, if so, the player lose. 
		# Return board.status = 'ended'
		# board.player....concluded = true 
		# board.win_info = {
			# winner = {
				# name = 'String'
				# score = 'Score'
			# }
			# loser = {
				# name = 'String'
				# score = 'Score'
			# }
			# 
		# }
	# else
		# Return
		# board.player....concluded = true 



@socketio.on('end')
def end_game(packed):
	uuid = packed["uuid"]
	data = db.find_one({'_id':uuid})
	data["status"] = "ended"
	db.replace_one({'_id': data['_id']},data)
	emit('on_game_loop', data, room=uuid)

if __name__ == '__main__':
	thread = socketio.start_background_task(target = subscribe)
	LOG.info("starting server... accepting connection")
	socketio.run(app, host="0.0.0.0", port=5000)
