#!/bin/bash

# Env
kubectl delete configmap final-config-env-file
kubectl create configmap final-config-env-file \
        --from-env-file=env.yml

#Front Deploy
kubectl delete deployment front
kubectl create -f ./deploy-front.yml

#Front Service
kubectl delete svc/front-service
kubectl create -f ./svc-front.yml


#Redis Deploy
kubectl delete deployment redis
kubectl create -f ./deploy-redis.yml

#Redis Service
kubectl delete svc/redis-service
kubectl create -f ./svc-redis.yml

#Mongo Controller,pvc
kubectl delete rc/mongo-controller
kubectl delete pvc mongo-pv-claim
kubectl delete deployment mongo
kubectl create -f ./deploy-mongo.yml

#Mongo Service
kubectl delete svc/mongo-service
kubectl create -f ./svc-mongo.yml

#Webapp Deploy
kubectl delete deployment webapp
kubectl create -f ./deploy-webapp.yml

#Webapp Service
kubectl delete svc/webapp
kubectl create -f ./svc-webapp.yml

#Worker Deploy 
kubectl delete deployment worker
kubectl create -f ./deploy-worker.yml

#deploy pv
kubectl delete -f ./pv1.yml
kubectl create -f ./pv1.yml




