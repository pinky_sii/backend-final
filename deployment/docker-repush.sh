#!/bin/bash

docker tag backend-final_validate-worker teamata/final_worker 
docker push teamata/final_worker 

docker tag backend-final_controller teamata/final_webapp 
docker push teamata/final_webapp 

docker tag backend-final_client teamata/final_front
docker push teamata/final_front
