import axios from 'axios';

const URL = 'http://puyo-paper-io.teamata.me:30005';
// const URL = 'http://0.0.0.0:5000';

export default () => axios.create({
  baseURL: URL,
  // headers: {
  //   Accept: 'application/json',
  //   'Content-Type': 'application/json',
  // },
});
