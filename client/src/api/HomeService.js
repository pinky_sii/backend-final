import Api from '@/api/Api';

export default {
  hostGame(username) {
    const path = '/host';
    const data = {
      name: username
    };
    return Api().post(path, data)
  },

  joinGame(uuid,username) {
    const path = '/join';
    const data = {
      uid: uuid,
      name: username,
    };
    return Api().post(path, data)
  },

  rollDie() {
    const path = '/roll';
    return Api().get(path)
  }
}