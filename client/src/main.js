import 'buefy/dist/buefy.css';
import Buefy from 'buefy';
import Vue from 'vue'
import App from './App.vue'
import router from './router';
import socketio from 'socket.io-client';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faCoffee, faArrowLeft } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

library.add(faCoffee, faArrowLeft);

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(Buefy, {
  defaultIconPack: "fa far fas fad fal"
});

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
