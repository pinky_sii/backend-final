import os
import logging
import json
import uuid
import redis
import hashlib
import sys
import shutil
import concurrent.futures
import math
from itertools import repeat
from functools import partial
from multiprocessing import Pool, cpu_count

LOG = logging

INSTANCE_NAME = uuid.uuid4().hex

LOG.basicConfig(
    level=LOG.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)

REDIS_QUEUE_LOCATION = os.getenv('REDIS_QUEUE', 'redis-service')

class RedisResource:
    REPORT_QUEUE = 'queue:report'
    QUEUE_NAME = 'queue:validate'

    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    conn = redis.Redis(host=host, *port)

def watch_queue(redis_conn, queue_name, callback_func, timeout=30):
    active = True

    while active:
        # Fetch a json-encoded task using a blocking (left) pop
        packed = redis_conn.blpop([queue_name], timeout=timeout)

        if not packed:
            # if nothing is returned, poll a again
            continue

        _, packed_task = packed

        # If it's treated to a poison pill, quit the loop
        if packed_task == b'DIE':
            active = False
        else:
            task = None
            try:
                task = json.loads(packed_task)
            except Exception:
                LOG.info(packed_task)
                LOG.exception('json.loads failed')
            if task:
                LOG.info(task)
                callback_func(task)

"""
    Check for overlapping where 
    - placing_box is the box from current player
    - target_box is the box on the board that will get computed 
    return a boolean indicating overlapping. 

"""

# !(i > bi + by - 1 || i + y - 1 < bi)

def check_placeable(placing_box, target_box):
    target_box_i, target_box_j, target_box_x, target_box_y = target_box 
    placing_box_i, placing_box_j, placing_box_x, placing_box_y = placing_box
    # LOG.info(f"want to place box of : {placing_box}")
    # LOG.info(f"check collision with : {target_box}")
    # LOG.info(str((placing_box_j > target_box_j + target_box_x-1)))
    # LOG.info(str((placing_box_j + placing_box_x - 1 < target_box_j)))
    # LOG.info(str((placing_box_i > target_box_i + target_box_y - 1)))
    # LOG.info(str((placing_box_i + placing_box_y - 1 < target_box_i)))
    if not((placing_box_j > target_box_j + target_box_x-1) or (placing_box_j + placing_box_x - 1 < target_box_j)) and not ((placing_box_i > target_box_i + target_box_y - 1) or (placing_box_i + placing_box_y - 1 < target_box_i)):        
        LOG.info("collide!")
        return False
    return True 

"""
    get neighbors coordinate 
"""
def get_neighbors(coordinate):
    # LOG.info(f"coordinate : {coordinate}")
    i, j = coordinate
    return [[i+1,j],[i-1,j],[i,j+1],[i,j-1]]

"""
    return true if this box is next to its own boxes 
"""
def check_next_to_edge(placing_box, edges, board_size_vertical, board_size_horizontal):
    placing_box_i, placing_box_j, placing_box_x, placing_box_y = placing_box
    # LOG.info("in check_next_to_edge")
    # LOG.info(f"placing box is : {placing_box}")
    # LOG.info(f"range {placing_box_i} : {placing_box_i + placing_box_y}")
    for i in range(placing_box_i, placing_box_i + placing_box_y):
        for j in range(placing_box_j, placing_box_j + placing_box_x):
            # LOG.info("check neighbors")
            neighbors = get_neighbors((i,j))
            real_neighbors = list(filter(lambda x : find_edges(x, edges), neighbors))
            if len(real_neighbors) > 0:
                update_edges(placing_box, edges, board_size_vertical, board_size_horizontal)
                return True
    return False 

"""
    find respective nearing boxes. 
"""
def find_edges(coordinate, edges):
    i,j = coordinate 
    tmp_list = list(filter(lambda x: x[0] == i and x[1] == j, edges))
    return len(tmp_list) > 0 

"""
    check if old edges are still valid as a proper edge
"""
def update_old_edges(edges, neighbors, i, j, board_size_vertical, board_size_horizontal, new_point):
    for neigh in neighbors: 
        # LOG.info(f"neigh {neigh}")
        # LOG.info(f"edges {edges}")
        tmp_index = [a for a in range(len(edges)) if neigh[0] == edges[a][0] and neigh[1] == edges[a][1]]
        # LOG.info(tmp_index)
        # tmp_list = list(filter(lambda e : e[0] == neigh[0] and e[1] == neigh[1], edges))
        if len(tmp_index) > 0:
            idx = tmp_index[0]
            new_point[2] -= 1 
            # LOG.info(edges[idx])
            edges[idx][2] -= 1
            if edges[idx][2] < 1:
                del edges[idx]
    if ((i == 0) and (i == j)) or ((i == board_size_vertical-1) and (i == j)):
        new_point[2] += 2
    elif j == 0 or i == 0 or i == board_size_vertical-1 or j == board_size_horizontal-1 :
        new_point[2] += 3
    else:
        new_point[2] += 4
    if new_point[2] > 1:
        edges.append(new_point)

"""
    update edge correspondingly to the new placement. 
"""
def update_edges(placing_box, edges, board_size_vertical, board_size_horizontal):
    placing_box_i, placing_box_j, placing_box_x, placing_box_y = placing_box
    for i in range(placing_box_i, placing_box_i + placing_box_y):
        for j in range(placing_box_j, placing_box_j + placing_box_x):
            # LOG.info("updating edges")
            new_point = [i, j ,0]
            neighbors = get_neighbors((i,j))
            update_old_edges(edges, neighbors, i, j, board_size_vertical, board_size_horizontal, new_point)    
 
def validate(log, board):
    turn = board.get('turn')
    from_player = board.get('from_player')
    player_one = board.get('player_one')
    player_two = board.get('player_two')
    player_one_boxes = player_one["blocks"]
    player_two_boxes = player_two["blocks"]
    # placing_box = (1,0,6,6)
    placing_box = (board.get("placing_block").get("i"), board.get("placing_block").get("j"), board.get("placing_block").get("x"), board.get("placing_block").get("y"))
    target_boxes = list(map(lambda each_block : ((each_block.get("i"), each_block.get("j"), each_block.get("x"), each_block.get("y"))), player_one_boxes + player_two_boxes ))
    is_placeble = list(filter(lambda box: not check_placeable(placing_box, box), target_boxes))
    #the block is placeble
    data = {}
    data["_id"] = board.get("_id")
    data["player_one"] = player_one
    data["player_two"] = player_two
    data["turn"] = turn 
    if len(is_placeble) < 1:
        LOG.info(f" ---- {placing_box} has no collosion ... checking valid edge ---- ")
        edges = None 
        if turn == "player_one":
            edges = player_one["edges"]
        else:
            edges = player_two["edges"]
        if check_next_to_edge(placing_box, edges, 18, 27): #if valid 
            LOG.info(f"---- {placing_box} have no collision and valid! proceed to update ----")
            data[turn]["blocks"].append(board.get("placing_block"))
            data[turn]["score"] += board.get("placing_block")["x"]*board.get("placing_block")["y"]
            data["turn"] = "player_two" if turn == "player_one" else "player_one"
    json_packed = json.dumps(data)
    RedisResource.conn.rpush(
        RedisResource.REPORT_QUEUE,
        json_packed)
            



def main():
    try:
        workers = cpu_count()
    except NotImplementedError:
        workers = 1
    pool = Pool(processes=workers)
    result = pool.map(check_overlapping, repeat(TARGET_PLACE), BOXES)

    # with concurrent.futures.ProcessPoolExecutor() as executor:
    #     for box, result in zip(BOXES, executor.map(check_overlapping, BOXES, repeat(TARGET_PLACE))):
    #         # print(f" result : {box} & {result}")
    #         pass




    # with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
    # Start the load operations and mark each future with its URL
        # future_to_compute = {executor.submit(check_overlapping, TARGET_PLACE, box): box for box in BOXES}
        # for future in concurrent.futures.as_completed(future_to_compute):
        #     result = future_to_compute[future]
        #     try:
        #         data = future.result()
        #     except Exception as exc:
        #         print('%r generated an exception: %s' % (result, exc))
        #     else:
        #         pass
                # print("AH")
                # print(f" result : {result}")
    # return 
 


# def stupid_main():
#     for item in BOXES:
#         check_overlapping(PLACING_BOX, item)
#     return 
if __name__ == '__main__':
    # main()
   #  stupid_main()
    LOG.info('Starting a worker...')
    LOG.info('Unique name: %s', INSTANCE_NAME)
    host, *port_info = REDIS_QUEUE_LOCATION.split(':')
    LOG.info(f"Redis host: {host}")
    port = tuple()
    if port_info:
        port, *_ = port_info
        port = (int(port),)

    named_logging = LOG.getLogger(name=INSTANCE_NAME)
    named_logging.info('Trying to connect to %s [%s]', host, REDIS_QUEUE_LOCATION)
    redis_conn = redis.Redis(host=host, *port)
    watch_queue(
        redis_conn, 
        RedisResource.QUEUE_NAME, 
        lambda task_descr: validate(named_logging, task_descr))



